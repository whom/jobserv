 
#!/bin/sh
pwd

# get CNs
read -p "Enter Server CN (default: localhost): " SRVNAME
read -p "Enter Client CN (default: localhost): " CLTNAME
if [ -z "$SRVNAME" ]; then
    SRVNAME=localhost
fi

if [ -z "$CLTNAME" ]; then
    CLTNAME=localhost
fi


SERVER_CA_CN=jobserv-server-ca
SERVER_PATH=resources/server
CLIENT_CA_CN=jobserv-client-ca
CLIENT_PATH=resources/client
TEST_CA_CN=jobserv-bad-cert-ca
TEST_CN=localhost
TEST_PATH=resources/test

# refactor this to test for directory existanc
rm -rf resources
mkdir resources/
mkdir resources/client
mkdir resources/server
mkdir resources/test
rm -rf staging


# Get passwords for CAs
read -p "Enter Server CA Passphrase: " SRVCAPASS
read -p "Enter Client CA Passphrase: " CLTCAPASS
if [ -z "$SRVCAPASS" ]; then
    SRVCAPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)
    echo "[*] Server CA Password is: " $SRVCAPASS
fi

if [ -z "$CLTCAPASS" ]; then
    CLTCAPASS=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)
    echo "[*] Client CA Password is: " $CLTCAPASS
fi

# Generate CA Keys
echo "[+] Generating Server CA Key"
openssl genrsa -passout pass:$SRVCAPASS -aes256 -out $SERVER_PATH/ca.key 4096
echo "[+] Generating Client CA Key"
openssl genrsa -passout pass:$CLTCAPASS -aes256 -out $CLIENT_PATH/ca.key 4096
echo "[+] Generating test CA Key"
openssl genrsa -passout pass:dontusethiskey -aes256 -out $TEST_PATH/ca.key 4096

# Generate CA Certs
echo "[+] Generating Server CA Cert"
openssl req -passin pass:$SRVCAPASS -new -x509 -days 365 -key $SERVER_PATH/ca.key -out $SERVER_PATH/ca.crt -subj "/CN=${SERVER_CA_CN}"
echo "[+] Generating Client CA Cert"
openssl req -passin pass:$CLTCAPASS -new -x509 -days 365 -key $CLIENT_PATH/ca.key -out $CLIENT_PATH/ca.crt -subj "/CN=${CLIENT_CA_CN}"
echo "[+] Generating test CA Key"
openssl req -passin pass:dontusethiskey -new -x509 -days 365 -key $TEST_PATH/ca.key -out $TEST_PATH/ca.crt -subj "/CN=${TEST_CA_CN}"


# Generate Server Key, Signing request, cert
echo "[+] Generating Server key"
openssl genrsa -passout pass:${SRVCAPASS} -aes256 -out $SERVER_PATH/private.key 4096
echo "[+] Generating Server signing request"
openssl req -passin pass:${SRVCAPASS} -new -key $SERVER_PATH/private.key -out $SERVER_PATH/request.csr -subj "/CN=${SRVNAME}"
echo "[+] Generating Server certificate "
openssl x509 -req -passin pass:${SRVCAPASS} -days 365 -in $SERVER_PATH/request.csr -CA $SERVER_PATH/ca.crt -CAkey $SERVER_PATH/ca.key -set_serial 01 -out $SERVER_PATH/server.crt
echo "[+] Removing passphrase from server key"
openssl rsa -passin pass:${SRVCAPASS} -in $SERVER_PATH/private.key -out $SERVER_PATH/private.key

# Generate Client Key, Signing request, cert
echo "[+] Generating Client key"
openssl genrsa -passout pass:${CLTCAPASS} -aes256 -out $CLIENT_PATH/private.key 4096
echo "[+] Generating Client signing request"
openssl req -passin pass:${CLTCAPASS} -new -key $CLIENT_PATH/private.key -out $CLIENT_PATH/request.csr -subj "/CN=${CLTNAME}"
echo "[+] Generating Client certificate "
openssl x509 -req -passin pass:${CLTCAPASS} -days 365 -in $CLIENT_PATH/request.csr -CA $CLIENT_PATH/ca.crt -CAkey $CLIENT_PATH/ca.key -set_serial 01 -out $CLIENT_PATH/client.crt
echo "[+] Removing passphrase from client key"
openssl rsa -passin pass:${CLTCAPASS} -in $CLIENT_PATH/private.key -out $CLIENT_PATH/private.key

# Generate Test Key, Signing request, cert
echo "[+] Generating test key"
openssl genrsa -passout pass:dontusethiskey -aes256 -out $TEST_PATH/private.key 4096
echo "[+] Generating test signing request"
openssl req -passin pass:dontusethiskey -new -key $TEST_PATH/private.key -out $TEST_PATH/request.csr -subj "/CN=${TEST_CN}"
echo "[+] Generating test certificate "
openssl x509 -req -passin pass:dontusethiskey -days 365 -in $TEST_PATH/request.csr -CA $TEST_PATH/ca.crt -CAkey $TEST_PATH/ca.key -set_serial 01 -out $TEST_PATH/test.crt
echo "[+] Removing passphrase from test key"
openssl rsa -passin pass:dontusethiskey -in $TEST_PATH/private.key -out $TEST_PATH/private.key


echo "[+] Converting private keys to X.509"
openssl pkcs8 -topk8 -nocrypt -in $CLIENT_PATH/private.key -out $CLIENT_PATH/private.pem
openssl pkcs8 -topk8 -nocrypt -in $SERVER_PATH/private.key -out $SERVER_PATH/private.pem
openssl pkcs8 -topk8 -nocrypt -in $TEST_PATH/private.key -out $TEST_PATH/private.pem

