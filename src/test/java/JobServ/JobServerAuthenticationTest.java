/*
 * JobServerAuthenticationTest
 *
 * v1.0
 *
 * May 21, 2019
 */

package JobServ;

import java.io.File;
import javax.net.ssl.SSLException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalAnswers.delegatesTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import io.grpc.ManagedChannel;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import io.grpc.testing.GrpcCleanupRule;
import io.grpc.netty.GrpcSslContexts;

import io.netty.handler.ssl.ClientAuth;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslProvider;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;

/*
 * JobServerAuthenticationTest
 * Creates a client using authorized certs and another one using unauthorized certs
 * Ensures only the client with authorized certs can connect to the server.
 * For more information on the hardcoded paths check buildwrapper.sh
 */

@RunWith(JUnit4.class)
public class JobServerAuthenticationTest {

    private final String projectRoot = "";

    // Authorized client key/cert/ca
    private final String clientCa = projectRoot + "resources/client/ca.crt";
    private final String clientKey = projectRoot + "resources/client/private.pem";
    private final String clientCert = projectRoot + "resources/client/client.crt";

    // Authorized server key/cert/ca
    private final String serverCa = projectRoot + "resources/server/ca.crt";
    private final String serverKey = projectRoot + "resources/server/private.pem";
    private final String serverCert = projectRoot + "resources/server/server.crt";

    // controlled failure key/cert/ca
    private final String badCa = projectRoot + "resources/test/ca.crt";
    private final String badKey = projectRoot + "resources/test/private.pem";
    private final String badCert = projectRoot + "resources/test/test.crt";

    // badClient uses unauthorized certs
    private JobServClientAPIConnector goodClient;
    private JobServClientAPIConnector badClient;
    private JobServServer server;

    // was setUp able to use SSL Certs
    private Boolean serverSslInitialized = true;
    private Boolean clientSslInitialized = true;

    /*
     * test constructor
     * generates both clients and the server
     */
    public JobServerAuthenticationTest() throws Exception {

        try {
            // generate SSL contexts
            SslContextBuilder serverContextBuilder = SslContextBuilder.forServer(new File(serverCert),
                                                                                 new File(serverKey));
            serverContextBuilder.trustManager(new File(clientCa));
            serverContextBuilder.clientAuth(ClientAuth.REQUIRE);

            this.server = new JobServServer(GrpcSslContexts.configure(serverContextBuilder).build(), 8448);
            this.serverSslInitialized = true;

        } catch (SSLException e) {
            this.serverSslInitialized = false;
            System.err.println(e.getMessage());

        } catch (IOException e) {
            this.serverSslInitialized = false;
            System.err.println(e.getMessage());
        }

        // generate ssl for clients
        if (this.serverSslInitialized) {
            try {
                SslContextBuilder goodClientBuilder = GrpcSslContexts.forClient();
                goodClientBuilder.trustManager(new File(serverCa));
                goodClientBuilder.keyManager(new File(clientCert),  new File(clientKey));

                SslContextBuilder badClientBuilder = GrpcSslContexts.forClient();
                badClientBuilder.trustManager(new File(serverCa));
                badClientBuilder.keyManager(new File(badCert), new File(badKey));

                ManagedChannel goodChannel = NettyChannelBuilder.forAddress("localhost", 8448)
                    .sslContext(goodClientBuilder.build())
                    .directExecutor()
                    .build();

                ManagedChannel badChannel = NettyChannelBuilder.forAddress("localhost", 8448)
                    .sslContext(badClientBuilder.build())
                    .directExecutor()
                    .build();

                goodClient = new JobServClientAPIConnector(goodChannel);
                badClient = new JobServClientAPIConnector(badChannel);
                this.clientSslInitialized = true;

            } catch (SSLException e) {
                this.clientSslInitialized = false;
                System.err.println(e.getMessage());
            }
	    
        } else {
            this.clientSslInitialized = false;
        }
    }

    /*
     * TLS Cert Auth Test
     * this needed to be one test because running multiple tests at the same time 
     * fails as the server tries to rebind to the same port.
     */
    @Test
    public void certificateAuthenticationTest() {
        assertEquals(true, serverSslInitialized);
        assertEquals(true, clientSslInitialized);
	
        int result = badClient.sendNewJobMessage("test command");
        assertEquals(-3, result);

        result = goodClient.sendNewJobMessage("test command");
        Boolean assertCondition = result == -3;
        assertEquals(assertCondition, false);
    }
}
