/*
 * ProcessManagerTest
 *
 * v1.0
 *
 * May 22, 2019
 */

package JobServ;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.concurrent.Future;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

/*
 * ProcessManagerTest
 * Class that performs positive and negative unit tests
 * of every public method in ProcessManager. This not
 * only unit tests ProcessManager but also integration
 * tests it with ProcessController.
 */
public class ProcessManagerTest {
    private ProcessManagerTestImplementation manager = new ProcessManagerTestImplementation();
    private ExecutorService threadPool = Executors.newCachedThreadPool();

    private int asyncTestPid;

    // calls a test function that simulates load by holding the lock for a long time
    private Callable<Object> holdLockFourSeconds = new Callable<Object>() {
            public Object call() {
                manager.longCallHoldsLock(asyncTestPid);
                return true;
            }
        };

    /*
     * addProcessTest()
     * positive unit test for newProcess
     */
    @Test
    public void addProcessesTest() {
        int pid = manager.newProcess("sleep 1");
        assertNotEquals(-1, pid);

        manager.shutdown();
    }

    /*
     * getStatusTest
     * unit test for getStatus
     */
   @Test
   public void getStatusTest() {
       int pid = manager.newProcess("sleep 1");
       int status = manager.getProcessStatus(pid);
       assertEquals(0, status);

       manager.shutdown();
   }

   /*
    * getOldStatusTest
    * do finished processes return 1
    */
    @Test
    public void getOldStatusTest() {
        int pid = manager.newProcess("echo 'test'");

        try{
            Thread.sleep(200);
        } catch (InterruptedException e) {
            //
        }

        int status = manager.getProcessStatus(pid);
        assertEquals(1, status);

        manager.shutdown();
    }

    /*
     * getUnknownStatusTest()
     * ensures 2 is returned when a status is not known
     */
    @Test
    public void getUnknownStatusTest() {
        int status = manager.getProcessStatus(400);
        assertEquals(3, status);
    }

    /*
     * getReturnTest()
     * test of process returns
     */
    @Test
    public void getReturnTest() {
        int pid = manager.newProcess("sleep .5");
        int ret = manager.getProcessReturn(pid);
        assertEquals(256, ret);

        try {
            Thread.sleep(550);
        } catch (InterruptedException e) {
            //
        }

        ret = manager.getProcessReturn(pid);
        assertNotEquals(ret, 256);
        assertNotEquals(ret, 257);
        assertNotEquals(ret, 258);

        manager.shutdown();
    }

    /*
     * getUNknownProcessReturn
     * tests process return for unknown processes
     */
    @Test
    public void getUnknownProcessReturnTest() {
        int ret = manager.getProcessReturn(502);
        assertEquals(258, ret);
        manager.shutdown();
    }

    /*
     * getProcessOutputTest()
     * verifies output is grabbed correctly from processes
     */
    @Test
    public void getProcessOutputTest() {
        int pid = manager.newProcess("echo test");

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            //
        }

        String out = manager.getProcessOutput(pid, 2);
        assertEquals("test\n", out); // calls string.equals()

        manager.shutdown();
    }


    /*
     * getUnknownOutputTest()
     * verifies correct information is returned when
     * output is requested of an unknown process
     */
    @Test
    public void getUnknownOutputTest() {
        String out = manager.getProcessOutput(532, 10);
        assertEquals("[-] SERVER: Process not found", out);
        manager.shutdown();
    }

    /*
     * killProcessTest()
     * ensures killing a process works
     * also tests if getProcessStatus returns 2
     */
    @Test
    public void killProcessTest() {
        int pid = manager.newProcess("sleep 10");
        int ret = manager.killProcess(pid);

        assertEquals(1, ret);

        int status = manager.getProcessStatus(pid);

        assertEquals(2, status);

        manager.shutdown();
    }

    /*
     * asyncLockTimeoutTest
     * ensures that two things cannot grab the lock at the same time
     */
    @Test
    public void asyncLockTimeoutTest() {
        // start new process that will last the whole test
        asyncTestPid = this.manager.newProcess("sleep 7");
        int secondProcess = this.manager.newProcess("sleep 10");

        // grab that processes lock for 4 seconds
        Future<Object> future = this.threadPool.submit(this.holdLockFourSeconds);

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            System.err.println("[!!] Thread for async test interrupted!");
        }

        // Try to grab a held lock
        System.err.println("[2] attempting to grab (held) lock");
        int status = this.manager.getProcessStatus(this.asyncTestPid);
        assertEquals(4, status); // should time out after 2 secs

        // try to grab unrelated lock (not nessesary, but important it works)
        int statusTertiary = this.manager.getProcessStatus(secondProcess);
        assertNotEquals(4, statusTertiary);

        // give lockMap small time to update
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            System.err.println("[!!] Thread for async test interrupted!");
        }

        // should be grabbable now
        int statusSecondTry = this.manager.getProcessStatus(this.asyncTestPid);
        assertNotEquals(4, statusSecondTry);

        manager.shutdown();
    }
}

