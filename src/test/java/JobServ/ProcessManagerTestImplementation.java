/*
 * ProcessManagerTestImplementation
 *
 * v1.0
 *
 * May 23, 2019
 */


package JobServ;

import java.util.concurrent.TimeoutException;

/*
 * ProcessManagerTestImplementation
 * inherits ProcessManager and adds useful functions for testing
 */
class ProcessManagerTestImplementation extends ProcessManager {

    public void longCallHoldsLock(int pid) {
        try {
            super.getLock(pid);
            System.err.println("[1] Long Call Has Lock");

            // hold lock for 3.5 seconds, more than double normal timeout.
            Thread.sleep(3500);

            super.releaseLock(pid);

        } catch (InterruptedException e) {
            super.releaseLock(pid); // this doesnt happen, dont cancel this task
            System.err.println("[3] Released lock: interrupted");
            return;
        }
    }
}
