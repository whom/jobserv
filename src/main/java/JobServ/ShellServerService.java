/*
 * ShellServerService
 *
 * v1.0
 *
 * May 18, 2019
 */

package JobServ;
import io.grpc.stub.StreamObserver;

/*
 * The ShellServerService wraps around the protobuf API
 * Implements API endpoints
 */
class ShellServerService extends ShellServerGrpc.ShellServerImplBase {

    private ProcessManager manager;

    /*
     * constructor
     * initialized ProcessManager
     */
    public ShellServerService(ProcessManager manager) {
        this.manager = manager;
    }

    /*
     * getStatus
     * implements api endpoint as defined in jobserv.proto
     */
    @Override
    public void getStatus(PIDMessage request,
                          StreamObserver<StatusMessage> responder) {

        JobServServer.logger.write("New status request for pid: " + String.valueOf(request.getPid()));
        int status = manager.getProcessStatus(request.getPid());

        StatusMessage reply = StatusMessage.newBuilder()
            .setProcessStatus(status)
            .build();
        responder.onNext(reply);
        responder.onCompleted();
    }

    /*
     * getOutput
     * implements api endpoint as defined in jobserv.proto
     */
    @Override
    public void getOutput(OutputRequestMessage request,
                          StreamObserver<OutputMessage> responder) {

        JobServServer.logger.write("New Output request for pid: " + String.valueOf(request.getPid()));
        String output = manager.getProcessOutput(request.getPid(),
                                                 request.getLines());

        OutputMessage reply = OutputMessage.newBuilder()
            .setOutput(output)
            .build();
        responder.onNext(reply);
        responder.onCompleted();
    }

    /*
     * newJob
     * implements api endpoint as defined in jobserv.proto
     */
    @Override
    public void newJob(NewJobMessage request,
                       StreamObserver<PIDMessage> responder) {

        String command = request.getCommand();
        JobServServer.logger.write("New job request: " + command);
        int newPid = manager.newProcess(command);

        PIDMessage reply = PIDMessage.newBuilder()
            .setPid(newPid)
            .build();
        responder.onNext(reply);
        responder.onCompleted();
    }

    /*
     * getReturn
     * implements api endpoint as defined in jobserv.proto
     */
    @Override
    public void getReturn(PIDMessage request,
                          StreamObserver<ReturnMessage> responder) {

        JobServServer.logger.write("New request for return from job: " + String.valueOf(request.getPid()));
        int retVal = manager.getProcessReturn(request.getPid());

        ReturnMessage reply = ReturnMessage.newBuilder()
            .setProcessReturnCode(retVal)
            .build();
        responder.onNext(reply);
        responder.onCompleted();
    }

    /*
     * killJob
     * implements api endpoint as defined in jobserv.proto
     */
    @Override
    public void killJob(PIDMessage request,
                        StreamObserver<StatusMessage> responder) {

        JobServServer.logger.write("New Request to kill job: " + String.valueOf(request.getPid()));
        int status = manager.killProcess(request.getPid());

        StatusMessage reply = StatusMessage.newBuilder()
            .setProcessStatus(status)
            .build();
        responder.onNext(reply);
        responder.onCompleted();
    }
}
