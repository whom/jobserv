/*
 * ProcessManager
 *
 * v1.0
 *
 * May 22, 2019
 */

package JobServ;

import java.util.concurrent.Future;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.HashMap;
import java.util.Iterator;
import java.io.IOException;

/*
 * ProcessManager
 * Holds a list of ProcessControllers and controls access to them via mutex
 * Mutex Timeout is declared here as well.
 */
class ProcessManager {
    // TODO: LOCK_TIMEOUT should be defined in a configuration management system
    private final int LOCK_TIMEOUT = 2; // seconds

    /*
     * The significance of the concurrent hash map is that an in process
     * update will not leave it in an unusable state like it will a normal
     * HashMap. It is still up to the programmer in this instance to make
     * sure that there are no concurrent operations done to the ProcessControllers
     * Themselves. The last thing we want is to throw NPEs or whatnot when
     * accessing a process destroyed mid read by another thread.
     * Hence getLock(...) and lockMap controlling access to individual entries in
     * processMap
     */
    protected ConcurrentHashMap<Integer, ProcessController> processMap;
    private ExecutorService threadPool = Executors.newCachedThreadPool();

    /*
     * Constructor
     * initializes process queue and start the background process checking daemon
     */
    public ProcessManager() {
        processMap = new ConcurrentHashMap<Integer, ProcessController>();
        /* TODO: In a long running server over a large period of time
         * It is possible that the streams used to redirect IO in the
         * Processes may become a significant use of resources.
         * In this case a background thread should be called to periodically
         * remove dead ProcessControllers after calling kill() on them.
         *
         * (grab lock, iterate over map, remove finished processes, store exit codes, release lock, sleep, repeat)
         */
    }

    /*
     * newProcess()
     * Takes a command and returns the translated pid of a new process
     * Returns -1 if controller throws an IOException
     */
    public int newProcess(String command) {

        try {
            ProcessController newProc = new ProcessController(command, this.LOCK_TIMEOUT);
            this.processMap.put(newProc.getPid(), newProc);
            return newProc.getPid();

        } catch (IOException e) {
            JobServServer.logger.write("Couldnt Spawn New Command: (" +
                                       command + "): " + e.getMessage());
            return -1;
        }
    }

    /*
     * getProcessStatus()
     * returns whether or not a process is running.
     * 0: running
     * 1: not running
     * 2: killed manually by a client
     * 3: doesnt exist
     * 4: couldnt grab lock
     */
    public int getProcessStatus(int pid) {
        try {
            if(!this.getLock(pid)) {
                // lock could not be grabbed before timeout
                JobServServer.logger.write("Timeout getting process status: " + String.valueOf(pid));
                return 4;
            }

        } catch (IndexOutOfBoundsException e) {
            return 3;
        }

        ProcessController candidate = this.processMap.get(pid);
        int status = candidate.getStatus();
        this.releaseLock(pid);
        return status;
    }

    /*
     * getProcessReturn()
     * returns:
     * 0-255: process exit code
     * 256: process still running
     * 257: process was killed by a client (TODO: list which client connection killed a process)
     * 258: process doesnt exist
     * 259: couldnt grab lock in time
     */
    public int getProcessReturn(int pid) {
        try {
            if(!this.getLock(pid)) {
                JobServServer.logger.write("Timeout getting process return: " + String.valueOf(pid));
                return 259;
            }

        } catch (IndexOutOfBoundsException e) {
            return 258;
        }

        ProcessController candidate = this.processMap.get(pid);
        int ret = candidate.getReturn();
        this.releaseLock(pid);
        return ret;
    }

    /*
     * getProcessOutput()
     * returns output of process 'pid'
     * or returns description of error
     */
    public String getProcessOutput(int pid, int lines) {
        try {
            if(!this.getLock(pid)) {
                JobServServer.logger.write("Timeout getting process output: " + String.valueOf(pid));
                return "[-] SERVER: Timeout grabbing lock to access process information";
            }

        } catch (IndexOutOfBoundsException e) {
            return "[-] SERVER: Process not found";
        }

        ProcessController candidate = this.processMap.get(pid);
        String output = candidate.getOutput(lines);
        this.releaseLock(pid);
        return output;
    }

    /*
     * killProcess()
     * returns mirror processStatus
     * returns 0 if still running
     * returns 1 if process was killed
     * returns 2 if process not found
     * returns 3 if couldnt grab lock
     */
    public int killProcess(int pid) {
        try {
            if(!this.getLock(pid)) {
                JobServServer.logger.write("Timeout killing process: " + String.valueOf(pid));
                return 3;
            }

        } catch (IndexOutOfBoundsException e) {

            return 2;
        }

        ProcessController candidate = this.processMap.get(pid);
        candidate.kill();
        this.releaseLock(pid);
        return 1;
    }

    /*
     * getLock()
     * Throws TimeoutException when it fails to get the lock.
     * Alternatively, throws false if lock doesnt exist for PID
     * Function is synchronized to prevent multiple threads accessing the same lock at once
     * (ConcurrentHashMap will report whatever lock value was last to successfully update)
     */
    protected synchronized Boolean getLock(int pid) throws IndexOutOfBoundsException {
        ProcessController candidate = this.processMap.get(pid);
        if (candidate == null) {
            throw new IndexOutOfBoundsException();
        }

        try {
            Boolean success = candidate.getLock();
            return success;

        } catch (InterruptedException e) {
            JobServServer.logger.write("[!] Couldnt get lock " +
                                       String.valueOf(pid) + ": "+ e.getMessage());
            return false;
        }

        /*
         * TODO: touch of tech debt here
         * There should honestly be an
         * operation retry queue for ops
         * That dont get the lock in time.
         *
         * This would require a scheduler
         * that manages a queue of callbacks
         * This scheduler would also likely
         * mediate access to the ProcessManager
         * object for fresh calls as well.
         */
    }

    /*
     * releaseLock()
     * releases mutex so other threads can operate on processqueue
     */
    protected void releaseLock(int pid) {
        ProcessController candidate = this.processMap.get(pid);
        if (candidate == null) {
            JobServServer.logger.write("Tried to release lock of process that doesnt exist!");
            return;
        }

        candidate.releaseLock();
    }

    /*
     * shutdown()
     * called (eventually) by the grpc shutdown hook
     * (AKA when user hits control c in the shell)
     * releases resources held in the processController objects
     */
    public void shutdown() {
        Iterator<HashMap.Entry<Integer, ProcessController>> iterator = this.processMap.entrySet().iterator();
        while (iterator.hasNext()) {
            HashMap.Entry<Integer, ProcessController> entry = iterator.next();

            entry.getValue().kill();
            iterator.remove();
        }
    }
}
