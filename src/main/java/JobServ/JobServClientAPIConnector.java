/*
 * JobServClientAPIConnector
 *
 * v1.0
 *
 * May 23, 2019
 */

package JobServ;

import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * JobServClientAPIConnector
 * Starts a connection to the API Connector
 * implements functions that send and recieve frm the API
 * Refactored into its own module to make the Client interface nicer
 * and to allow for a veriety of interfaces to be created
 */
class JobServClientAPIConnector {
    private final String apiFailureMessage = "Failed while trying to connect to server.";

    private static final Logger logger = Logger.getLogger(JobServClient.class.getName());

    private final ManagedChannel channel;

    /*
     * blockingStub is used when the client needs to block until the server responds
     * the client doesnt nessesarily need to support asynchronously firing off commands
     * in this shell-like interface it would be disconcerting to get multiple returns out of order
     */
    private final ShellServerGrpc.ShellServerBlockingStub blockingStub;

    /*
     * Constructor
     * Spawns a new blockingStub for network operations with the server
     */
    public JobServClientAPIConnector(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = ShellServerGrpc.newBlockingStub(this.channel);
    }

    /*
     * shutdown()
     * Gets called when you press cntrl+c
     * takes at most 5 seconds to close its connection
     */
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /*
     * getProcessOutput()
     * sends the server a request for output from the process identified by 'pid'
     * returns process output as string
     */
    public String getProcessOutput(int pid, int lines) {
        logger.info("[+] requesting output");

        OutputRequestMessage request = OutputRequestMessage.newBuilder()
            .setPid(pid)
            .setLines(lines)
            .build();
        OutputMessage response;

        try {
            // blocking network operation
            response = blockingStub.getOutput(request);

        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, this.apiFailureMessage + ": " + e.getStatus());
            return "<Error connecting to API>";
        }

        return response.getOutput();
    }

    /*
     * sendNewJobMessage()
     * sends a shell command to the api server
     * returns new pid of job
     * or -1 if server failed to create job
     * or -2 if client fails to connect
     */
    public int sendNewJobMessage(String command) {
        // thought of escaping this, but the vulnerability is only client side, from client user input.
        logger.info("[+] Sending command to server");

        NewJobMessage request = NewJobMessage.newBuilder()
            .setCommand(command)
            .build();
        PIDMessage response;

        try {
            // blocking network operation
            response = blockingStub.newJob(request);

        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING,  this.apiFailureMessage + ": " + e.getStatus());
            return -3;
        }

        return response.getPid();
    }

    /*
     * getProcessStatus()
     * requests running status of process pid
     * 0: running
     * 1: not running
     * 2: killed manually by a client
     * 3: doesnt exist
     * 4: couldnt grab lock
     */
    public int getProcessStatus(int pid) {
        logger.info("[+] Requesting status of a job");

        PIDMessage request = PIDMessage.newBuilder()
            .setPid(pid)
            .build();
        StatusMessage response;

        try {
            // blocking network operation
            response = blockingStub.getStatus(request);

        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, this.apiFailureMessage + ": " + e.getStatus());
            return -1;
        }

        return response.getProcessStatus();
    }

    /*
     * sends PID to server
     * returns process exit code
     * 0-255: process exit code
     * 256: process still running
     * 257: process was killed by a client
     * 258: process doesnt exist
     * 259: couldnt grab lock in time
     * 260: couldnt connect to API
     */
    public int getProcessReturn(int pid) {
        logger.info("[+] Requesting return code of a job");

        PIDMessage request = PIDMessage.newBuilder()
            .setPid(pid)
            .build();
        ReturnMessage response;

        try {
            // blocking network operation
            response = blockingStub.getReturn(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, this.apiFailureMessage + ": " + e.getStatus());
            return 260;
        }

        return response.getProcessReturnCode();
    }

    /*
     * killProcess()
     * send a PID to be killed, function returns process status after kill operation
     * returns 0 if still running
     * returns 1 if process was killed
     * returns 2 if process not found
     * returns 3 if couldnt grab lock
     * returns 4 on API failure
     */
    public int killProcess(int pid) {
        logger.info("[+] Killing a job");

        PIDMessage request = PIDMessage.newBuilder()
            .setPid(pid)
            .build();
        StatusMessage response;

        try {
            // blocking network operation
            response = blockingStub.killJob(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, this.apiFailureMessage + ": " + e.getStatus());
            return 4;
        }

        return response.getProcessStatus();
    }
}
