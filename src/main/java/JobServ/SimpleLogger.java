/*
 * SimpleLogger
 *
 * v1.0
 *
 * May 26, 2019
 */

package JobServ;

import java.io.File;
import java.sql.Timestamp;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

/*
 * SimpleLogger
 * Automatically manages the creation of and output to a log file
 * TODO: Log Levels, decorations for entries of different severity
 */
class SimpleLogger {
    private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    private Timestamp programStart;
    private FileWriter logWriter;
    private Boolean writable = true;

    /*
     * Constructor
     * Initializes timestamp and opens new file for logging
     */
    public SimpleLogger(String filePrefix) {
        this.programStart = new Timestamp(System.currentTimeMillis());
        File currentLog = new File(filePrefix + this.dateTimeFormat.format(this.programStart));

        try{
            this.logWriter = new FileWriter(currentLog, true);

        } catch (IOException e) {
            System.out.println("Error creating LogWriter!");
            this.writable = false;
        }

        this.write(this.programStart.toString() + ": JobServ Logging Started");
    }

    /*
     * write
     * appends a line of information to the log
     */
    public void write(String message) {
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        message = currentTime.toString() + "> " + message;

        if (this.writable) {
            try {
                this.logWriter.write(message + "\n");
                this.logWriter.flush();

            } catch (IOException e) {
                System.out.println(e.getMessage());
                this.writable = false;
            }
        }

        System.out.println(message);
    }

    /*
     * shutdown()
     * called on server exit, closes the FileWriter and frees its resources
     */
    public void shutdown() {
        Timestamp exitTime = new Timestamp(System.currentTimeMillis());
        this.write(exitTime.toString() + ": JobServ Logging Stopped");

        try {
            this.logWriter.close();

        } catch (IOException e) {
            // not sure what would be appropriate to do here
            System.out.println(e.getMessage());
        }
    }
}
