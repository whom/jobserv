/*
 * ProcessController
 *
 * v1.0
 *
 * May 22, 2019
 */

package JobServ;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/*
 * ProcessController
 * This class wraps a java Process object with metadata
 * such as translated PID that exist for this specific API
 * as well as general metadata like IO streams.
 */
class ProcessController {
    // incremented in constructor
    private static int nextPid = 0;
    private int pid;

    // TODO: add an api endpoint for streaming client input into
    // interactive processes (out of scope for initial API)
    private OutputStream output;
    private InputStream input;
    private InputStreamReader inputIntermediateStream;
    private BufferedReader reader;

    private Process process;
    private Boolean killedManually = false;

    private Lock lock;
    private int lockTimeout; // seconds

    /*
     * Constructor
     * Takes a command and spawns it in a new process
     * Redirects IO streams and assigns a fake PID
     */
    public ProcessController(String command, int lockTimeout) throws IOException {
        this.pid = ProcessController.nextPid;
        ProcessController.nextPid += 1;

        this.lock = new ReentrantLock();
        this.lockTimeout = lockTimeout;

        this.process = Runtime.getRuntime().exec(command);
        this.output = this.process.getOutputStream();
        this.input = this.process.getInputStream();
        this.inputIntermediateStream = new InputStreamReader(this.input);
        this.reader = new BufferedReader(this.inputIntermediateStream);

        JobServServer.logger.write("Job " + String.valueOf(this.pid) + ": " + command);
    }

    /*
     * getLock()
     * attempts to get the lock for lockTimeout seconds
     * or throws exceptions if interrupted
     */
    public boolean getLock() throws InterruptedException {
        return this.lock.tryLock(this.lockTimeout, TimeUnit.SECONDS);
    }

    /*
     * releaseLock()
     * releases lock on process
     */
    public void releaseLock() {
        try {
            this.lock.unlock();

        } catch (IllegalMonitorStateException e) {
            JobServServer.logger.write("Thread tried to release a lock it didnt have! " + e.getMessage());
        }
    }

    /*
     * getPid()
     * returns translated pid of this process
     */
    public int getPid() {
        return this.pid;
    }

    /*
     * getStatus()
     * returns whether or not the process is running
     */
    public int getStatus() {
        if (this.killedManually) {
            return 2;
        }

        try {
            process.exitValue();
            return 1;
        } catch (IllegalThreadStateException e) {
            return 0;
        }
    }

    /*
     * getReturn()
     * returns the exit code of the process
     * 256 if process is still running
     * 257 if process was killed manually and no longer exists
     * (unix/posix defines an exit code as a uint8, so 256+ is fair game)
     */
    public int getReturn() {
        if (this.killedManually) {
            return 257;
        }

        try {
            return process.exitValue();
        } catch (IllegalThreadStateException e) {
            return 256;
        }
    }

    /*
     * getOutput()
     * gets output from process
     */
    public String getOutput(int lines) {
        if(this.killedManually) {
            return "[-] SERVER: Process has already been killed by a JobServ client!";
	}

        String output = "";
        for (int i = 0; i < lines; i++) {
            String newLine = null;
            try {
                newLine = reader.readLine();
            } catch (IOException e) {
                newLine = "[-] SERVER: error reading process output: " + e.getMessage();
            } finally {
                if (newLine != null) {
                    output += newLine + "\n";
                }
            }

        }

        return output;
    }

    /*
     * kill()
     * Cleans up resources and destroys process
     */
    public void kill() {
        if (this.killedManually) {
            JobServServer.logger.write("Tried to kill already killed process");
            return;
        }

        try {
            this.input.close();
            this.output.close();
            this.inputIntermediateStream.close();
            this.reader.close();
            this.process.destroy();
            this.killedManually = true;
        } catch (IOException e) {
            JobServServer.logger.write("Killing process " +
                                       String.valueOf(this.pid) + " failed: " + e.getMessage());
        }
    }
}
