#!/bin/sh

# Ideally this next section would be done with gradle
# Unfortunately gradle's protobuf distribution plugin does not seem to have facilities to manually include certs 
# Or to specify seperate client and server tarballs for that matter
# Definitely more research on gradle should be done, but after JobServ hits MVP
echo "[+] extracting built code"
mkdir staging
mkdir staging/client
mkdir staging/server
mkdir staging/test

DIST_TAR=JobServ.tar
DIST_DIR=JobServ
if [ -f build/distributions/jobserv.tar ]; then
    DIST_TAR=jobserv.tar
    DIST_DIR=jobserv
fi

tar -xvf build/distributions/$DIST_TAR -C staging/client
tar -xvf build/distributions/$DIST_TAR -C staging/server
tar -xvf build/distributions/$DIST_TAR -C staging/test

echo "[+] removing server capabilities from client"
rm staging/client/$DIST_DIR/bin/jobserv-server staging/client/$DIST_DIR/bin/jobserv-server.bat

echo "[+] removing client capabilities from server"
rm staging/server/$DIST_DIR/bin/jobserv-client staging/server/$DIST_DIR/bin/jobserv-client.bat

echo "[+] populating certificates"
cp resources/server/server.crt staging/server/
cp resources/server/private.pem staging/server/
cp resources/client/ca.crt staging/server/
cp resources/client/client.crt staging/client/
cp resources/client/private.pem staging/client/
cp resources/server/ca.crt staging/client/
cp -r resources/* staging/test/

echo "[+] Adding wrapper script for client"
# This could also be a .desktop file without much more work.
cat << EOF > staging/client/client
    ./$DIST_DIR/bin/jobserv-client private.pem client.crt ca.crt \$@
EOF
chmod +x staging/client/client

echo "[+] Adding wrapper script for server"
# This could also be a .desktop file without much more work.
cat << EOF > staging/server/server
    ./$DIST_DIR/bin/jobserv-server \$1 server.crt private.pem ca.crt
EOF
chmod +x staging/server/server

echo "[+] removing test logs"
rm JobServ-Server-*
