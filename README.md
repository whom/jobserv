# JobServ
Remote Procedure Calls over the protobuf API

# Requirements
- openssl
- tar
- OpenJDK 8

# Building
Gradle will manage dependencies, generate code, compile the java, and package the code.
Simply run the folllowing command:
```shell
$ ./buildwrapper.sh
```
Buildwrapper will ask you for details about the client and server. If you are testing this software both CNs can be set to localhost.
Buildwrapper will then generate CAs for and signed certs for the Client and Server. In addition a seperate, third CA and cert will be generated for testing purposes.
Gradle will then generate protobuf source and compile it with the java source for the client and server.
After gradle is finished compiling and running the junit tests, buildwrapper will organize the sources with their respective certs in the staging folder.
In addition to a server folder and a client folder, there will be a test folder which has a copy of all certs and both server and client functionality.
The test CA is not trusted by the server or the client by default. As such, the test cert can be used to induce a mutual tls authentication failure.

# Running
After build, the programs can be found in the staging folder.
After changing directory to the 'staging/client' folder or the 'staging/server' folder, either program can be run as follows:

```
$ ./server (port)
$ ./client (hostname) (port) (command) (arguments)
```
For example:
```
$ ./buildwrapper.sh
 .....
$ cd staging/server
$ ./server 8448 &
$ cd ../client
$ client localhost 8448 new ping archive.org
```
alternatively, for guidance: 
```
$ ./server 
$ ./client help
```

# Distribution
At this point you can copy the staging/client or staging/server folders to any environment in which their Certificate CN's are valid.

# Testing
Running the gradle test task, or the buildwrapper will run all junit tests.
Currently that includes a test of certificate based authentication (Mutual TLS), tests for the thread safe process control module, and tests ensuring that only one connection can access a processes information at a time.

# Contributing
Many issues are marked great-first-issue for the sake of first time contributors.
If you are a more experienced contributor I encourage you to start on a different issue.

### Code Standards
Java contributions will be held to the following standard
https://www.oracle.com/technetwork/java/codeconvtoc-136057.html

Scala contributions are welcome as well, it is on the roadmap to refactor all this code to Scala.
